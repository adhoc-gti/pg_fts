require 'pg'

module PG::DB
  def databases(main_db = 'postgres')
    dbs = []

    conn = PG.connect(dbname: main_db)
    conn.exec('SELECT datname FROM pg_database') do |result|
      dbs = result.map { |row| row['datname'] }
    end
    conn.close

    dbs
  end

  def database?(name)
    databases.include?(name)
  end

  def dropdb(name, main_db = 'postgres')
    conn = PG.connect(dbname: main_db)
    conn.exec("DROP DATABASE #{name}")
    conn.close
  end

  def createdb(name, main_db = 'postgres')
    conn = PG.connect(dbname: main_db)
    conn.exec("CREATE DATABASE #{name}")
    conn.close
  end

  def dropdb!(name)
    dropdb(name) if database?(name)
  end

  def createdb!(name)
    dropdb!(name)
    createdb(name)
  end

  def createdb?(name)
    createdb(name) unless database?(name)
  end
end

PG.extend PG::DB

class Hash
  def symbolize_keys!
    each_key { |k| self[k.to_sym] = delete(k) }
  end

  def symbolize_keys
    dup.symbolize_keys!
  end
end

class String
  def lexemes
    scan(/'(.+?)':/).flatten
  end
end

module Schema
  def create_test_schema
    @schema = "test_#{(0...50).map { ('a'..'z').to_a[rand(26)] }.join}"
    exec "CREATE SCHEMA \"#{@schema}\";"
    exec "SET search_path TO \"#{@schema}\";"
  end

  def drop_test_schema
    exec 'SET client_min_messages TO WARNING;'
    exec "DROP SCHEMA \"#{@schema}\" CASCADE;"
    exec 'SET client_min_messages TO NOTICE;'
  end
end

module SQL
  attr_reader :conn

  def exec(query)
    conn.exec(query)
  end

  def create_table(table_name, desc)
    exec(SQL.create_table(table_name, desc))
  end

  def select(*fields, from: nil, where: nil, inner: nil, left: nil, right: nil)
    exec(SQL.select(*fields,
                    from: from,
                    where: where,
                    inner: inner,
                    left: left,
                    right: right))
  end

  def insert_into(table_name, *rows)
    exec(SQL.insert_into(table_name, *rows))
  end

  def update(table_name, set: nil, where: nil, inner: nil, left: nil, right: nil)
    exec(SQL.update(table_name, set: set, where: where, inner: inner, left: left, right: right))
  end

  def delete_from(table_name, where: nil, inner: nil, left: nil, right: nil)
    exec(SQL.delete_from(table_name, where: where, inner: inner, left: left, right: right))
  end

  def truncate(table_name)
    exec(SQL.truncate(table_name))
  end

  def count(*n)
    SQL.count(*n)
  end

  def join(table, on: nil)
    SQL.join(table, on: on)
  end

  def outer_join(table, on: nil)
    SQL.outer_join(table, on: on)
  end

  class Raw < String
    def as(n)
      Raw.new(self + " AS #{SQL.name(n)}")
    end

    def as?(n)
      n ? as(n) : self
    end

    def on(clause)
      Raw.new(self + " ON #{SQL.and_clause(clause)}")
    end

    def on?(clause)
      clause ? on(clause) : self
    end
  end

  class << self
    def raw(str)
      Raw.new(str)
    end

    def create_table(table_name, desc)
      <<-SQL
      CREATE TABLE #{SQL.name(table_name)} (
        #{SQL.list(desc.map { |k, v| "#{SQL.name(k)} #{v}" })}
      );
      SQL
    end

    def select(*fields, from: nil, where: nil, inner: nil, left: nil, right: nil)
      <<-SQL
      SELECT #{names(*fields)} FROM #{name(from)}
      #{SQL.inner?(inner)}
      #{SQL.left?(left)}
      #{SQL.right?(right)}
      #{SQL.where?(where)};
      SQL
    end

    def insert_into(table_name, *rows)
      <<-SQL
      INSERT INTO #{SQL.name(table_name)} (#{SQL.names(*rows.first.keys)})
      VALUES #{SQL.list(rows.map { |r| "(#{SQL.values(*r.values)})" })};
      SQL
    end

    def update(table_name, set: nil, where: nil, inner: nil, left: nil, right: nil)
      fail ArgumentError if set.nil?

      <<-SQL
      UPDATE #{SQL.name(table_name)}
      SET #{SQL.assign_clause(set)}
      #{SQL.inner?(inner)}
      #{SQL.left?(left)}
      #{SQL.right?(right)}
      #{SQL.where?(where)};
      SQL
    end

    def delete_from(table_name, where: nil, inner: nil, left: nil, right: nil)
      <<-SQL
      DELETE FROM #{SQL.name(table_name)}
      #{SQL.inner?(inner)}
      #{SQL.left?(left)}
      #{SQL.right?(right)}
      #{SQL.where?(where)};
      SQL
    end

    def truncate(table_name)
      <<-SQL
      TRUNCATE #{SQL.name(table_name)};
      SQL
    end

    ## Functions

    def count(*n)
      raw("COUNT(#{names(*n)})")
    end

    def join(table, on: nil)
      raw("JOIN #{name(table)}").on?(on)
    end

    def outer_join(table, on: nil)
      raw("OUTER JOIN #{name(table)}").on?(on)
    end

    ## Support

    def name(name)
      return name if name.is_a?(Raw)
      return raw('*') if name == '*'

      name.to_s.split('.').map { |e| "\"#{e}\"" }.join('.')
    end

    def names(*names)
      list(names.map { |k| name(k) })
    end

    def list(*items)
      items.join(', ')
    end

    def value(v)
      case v
      when Raw then v
      when String then raw "'#{v.tr("'", "''")}'"
      when Integer then raw v.to_s
      when nil then raw 'NULL'
      else fail NotImplementedError, v.inspect
      end
    end

    def values(*values)
      list(values.map { |v| value(v) })
    end

    def name_or_value(item)
      item.is_a?(Symbol) ? name(item) : value(item)
    end

    def equal(l, r)
      "#{name_or_value(l)} = #{name_or_value(r)}"
    end

    def assign_clause(clause)
      list(clause.map { |k, v| equal(k, v) })
    end

    def and_clause(clause)
      return clause if clause.is_a?(Raw) || clause.is_a?(String)

      clause.map do |e|
        case e
        when Array then "#{name(e[0])} = #{name_or_value(e[1])}"
        when Raw, String then e
        else fail NotImplementedError, e.class
        end
      end.join(' AND ')
    end

    def where?(clause)
      return "WHERE #{clause}" if clause.is_a?(Raw) || clause.is_a?(String)

      (clause && clause.any?) ? "WHERE #{SQL.and_clause(clause)}" : nil
    end

    def inner?(join)
      join ? "INNER #{join}" : nil
    end

    def left?(join)
      join ? "LEFT #{join}" : nil
    end

    def right?(join)
      join ? "RIGHT #{join}" : nil
    end
  end
end

module FTSAssertions
  def assert_count(expected, result)
    assert_equal(expected, Integer(result.first['count']))
  end

  def assert_id(expected, result)
    assert_equal(expected, Integer(result.first['id']))
  end

  def assert_lexeme_set(expected, tsv)
    lexemes = tsv.is_a?(String) ? tsv.lexemes : tsv

    assert_equal(expected && Set.new(expected), Set.new(lexemes))
  end

  def assert_lexemes(*expected, result)
    expected = expected.sort
    actual = result.map { |row| row['tsv'].lexemes }.sort!

    actual.each.with_index { |e, i| assert_lexeme_set(expected[i], e) }
  end

  def assert_tsv(*expected, where: nil)
    clauses = { from: PG::FTS.table, where: where }

    assert_count(expected.count, select(count('*').as(:count), **clauses))
    assert_lexemes(*expected, select('*', **clauses))
  end

  def read_fts
    cols = [:source_table, :source_id, :document_table, :document_id, :tsv]
    select(*cols, from: :fts).map do |row|
      row.to_h.merge('lexemes' => row['tsv'].lexemes.join(' '))
    end
  end

  def dump_fts
    h = format('%-12s | %-12s | %s', 'document', 'source', 'lexemes')
    b = read_fts.map do |row|
      format(
        '%-8s % 3d | %-8s % 3d | %s',
        row['document_table'],
        row['document_id'],
        row['source_table'],
        row['source_id'],
        row['lexemes'],
      )
    end

    "\n#{h}\n" << b.sort!.join("\n")
  end

  def assert_stable_fts
    before = dump_fts
    yield
    assert_equal(before, dump_fts)
  end

  def assert_trigger_names(expected)
    trigger_names = exec(<<-SQL).map { |r| r['name'] }
    SELECT tgname AS name
    FROM pg_trigger
    WHERE tgname LIKE '%_tsv'
    ORDER BY name;
    SQL

    assert_equal(expected, trigger_names)
  end

  def assert_procedure_names(expected)
    procedure_names = exec(<<-SQL).map { |r| r['name'] }
    SELECT routine_name AS name
    FROM information_schema.routines
    WHERE routine_name LIKE '%_tsv'
      AND routine_type = 'FUNCTION'
    ORDER BY name;
    SQL

    assert_equal(expected, procedure_names)
  end
end

class FTSTest < MiniTest::Test
  DB = 'test_pg_fts'.freeze

  include SQL
  include FTSAssertions
  include Schema

  def setup
    PG::FTS::Naming.hash_names! if ENV['HASH_NAMES']

    PG.createdb?(DB)
    @conn = PG.connect(dbname: DB)
    create_test_schema
    create_tables
    create_fts
  end

  def teardown
    drop_test_schema
    conn.close
  end
end
