require 'minitest/autorun'
require 'helper'
require 'pg/fts'

class TestFTSOnSelf < FTSTest
  def create_tables
    create_table :table1, {
      id: 'SERIAL',
      field1: 'VARCHAR(255)',
      field2: 'TEXT',
      field3: 'TEXT',
    }
  end

  def create_fts
    PG::FTS.create { |sql| exec sql }
  end

  def table1_fts_index
    @table1_fts_index ||= PG::FTS::Index::Self.new(
      'table1',
      'field1',
      'field2',
      'field3',
    )
  end

  def test_create
    table1_fts_index.create { |sql| exec sql }

    expected = %w(
      table1_from_table1_del_tsv
      table1_from_table1_ins_tsv
      table1_from_table1_trn_tsv
      table1_from_table1_upd_tsv
    )

    expected = %w(
      sha1_1c6793e412e1e3ceb85425c5fe0a7ded2db7ba2e_doc_del_tsv
      sha1_1c6793e412e1e3ceb85425c5fe0a7ded2db7ba2e_doc_ins_tsv
      sha1_1c6793e412e1e3ceb85425c5fe0a7ded2db7ba2e_doc_trn_tsv
      sha1_1c6793e412e1e3ceb85425c5fe0a7ded2db7ba2e_doc_upd_tsv
    ) if ENV['HASH_NAMES']

    assert_trigger_names(expected)
    assert_procedure_names(expected)
  end

  def test_drop_index
    table1_fts_index.create { |sql| exec sql }

    table1_fts_index.drop { |sql| exec sql }
    assert_trigger_names([])
    assert_procedure_names([])
  end

  def test_drop_all
    table1_fts_index.create { |sql| exec sql }

    PG::FTS::Index.drop_all { |sql| exec sql }
    assert_trigger_names([])
    assert_procedure_names([])
  end

  # scenario relaxed:1.1.
  def test_table1_index_on_document_insert
    exec table1_fts_index.on_document_insert_procedure
    exec table1_fts_index.on_document_insert_trigger

    insert_into :table1, {
      field1: 'one two three',
      field2: 'foo bar baz',
      field3: 'alpha beta gamma',
    }, {
      field1: 'three four five',
      field2: 'alice bob eve',
      field3: 'delta eta zeta',
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two),
               %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table1',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table1',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.2.
  def test_table1_index_on_document_update
    exec table1_fts_index.on_document_insert_procedure
    exec table1_fts_index.on_document_insert_trigger

    insert_into :table1, {
      field1: 'one two three',
      field2: 'foo bar baz',
      field3: 'alpha beta gamma',
    }, {
      field1: 'three four five',
      field2: 'alice bob eve',
      field3: 'delta eta zeta',
    }

    exec table1_fts_index.on_document_update_procedure
    exec table1_fts_index.on_document_update_trigger

    update :table1, set: { field1: 'seven eight nine' }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta eight foo gamma nine seven)

    assert_tsv %w(alpha bar baz beta eight foo gamma nine seven), where: {
      source_table: 'table1',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table1',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.3.
  def test_table1_index_on_document_delete
    exec table1_fts_index.on_document_insert_procedure
    exec table1_fts_index.on_document_insert_trigger

    insert_into :table1, {
      field1: 'one two three',
      field2: 'foo bar baz',
      field3: 'alpha beta gamma',
    }, {
      field1: 'three four five',
      field2: 'alice bob eve',
      field3: 'delta eta zeta',
    }

    exec table1_fts_index.on_document_delete_procedure
    exec table1_fts_index.on_document_delete_trigger

    delete_from :table1, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table1',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.4.
  def test_table1_index_on_document_truncate
    exec table1_fts_index.on_document_insert_procedure
    exec table1_fts_index.on_document_insert_trigger

    insert_into :table1, {
      field1: 'one two three',
      field2: 'foo bar baz',
      field3: 'alpha beta gamma',
    }, {
      field1: 'three four five',
      field2: 'alice bob eve',
      field3: 'delta eta zeta',
    }

    exec table1_fts_index.on_document_truncate_procedure
    exec table1_fts_index.on_document_truncate_trigger

    truncate :table1

    assert_tsv(*[])

    # TODO: test that only table1 entries get DELETEd
  end

  def test_table1_index_query
    exec table1_fts_index.on_document_insert_procedure
    exec table1_fts_index.on_document_insert_trigger

    insert_into :table1, {
      field1: 'one two three',
      field2: 'foo bar baz',
      field3: 'alpha beta gamma',
    }, {
      field1: 'three four five',
      field2: 'alice bob eve',
      field3: 'delta eta zeta',
    }

    assert_count(1, select(
      count('*').as(:count),
      from: :table1,
      inner: join(PG::FTS.table).on(
        :"#{PG::FTS.table}.document_id" => :'table1.id',
        :"#{PG::FTS.table}.document_table" => 'table1',
      ),
      where:
        %{"#{PG::FTS.table}"."tsv" @@ to_tsquery('simple', 'one & three')},
    ))

    assert_id(1, select(
      :'table1.id',
      from: :table1,
      inner: join(PG::FTS.table).on(
        :"#{PG::FTS.table}.document_id" => :'table1.id',
        :"#{PG::FTS.table}.document_table" => 'table1',
      ),
      where:
        %{"#{PG::FTS.table}"."tsv" @@ to_tsquery('simple', 'one & three')},
    ))

    assert_id(2, select(
      :'table1.id',
      from: :table1,
      inner: join(PG::FTS.table).on(
        :"#{PG::FTS.table}.document_id" => :'table1.id',
        :"#{PG::FTS.table}.document_table" => 'table1',
      ),
      where:
        %{"#{PG::FTS.table}"."tsv" @@ to_tsquery('simple', 'five & three')},
    ))

    assert_count(2, select(
      count('*').as(:count),
      from: :table1,
      inner: join(PG::FTS.table).on(
        :"#{PG::FTS.table}.document_id" => :'table1.id',
        :"#{PG::FTS.table}.document_table" => 'table1',
      ),
      where:
        %{"#{PG::FTS.table}"."tsv" @@ to_tsquery('simple', 'one | three')},
    ))
  end
end
