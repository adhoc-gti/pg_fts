require 'minitest/autorun'
require 'helper'
require 'pg/fts'

class TestFTS < FTSTest
  def create_tables; end

  def create_fts
    PG::FTS.create { |sql| exec sql }
  end

  def test_create
    # test if table exists and has requred columns
    select :document_table, :document_id, :source_table, :source_id, from: :fts
  end

  def test_duplicate
    insert_into :fts, {
      document_table: 'foo',
      document_id: 1,
      source_table: 'bar',
      source_id: 2,
    }
    assert_raises PG::UniqueViolation do
      insert_into :fts, {
        document_table: 'foo',
        document_id: 1,
        source_table: 'bar',
        source_id: 2,
      }
    end
  end

  def test_clear
    insert_into :fts, {
      document_table: 'foo',
      document_id: 1,
      source_table: 'bar',
      source_id: 2,
    }
    PG::FTS.clear { |sql| exec sql }
    assert_tsv(*[])
  end

  def test_drop
    PG::FTS.drop { |sql| exec sql }
    assert_raises PG::UndefinedTable do
      select '*', from: :fts
    end
  end
end
