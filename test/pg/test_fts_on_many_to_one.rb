require 'minitest/autorun'
require 'helper'
require 'pg/fts'

class TestFTSOnManyToOne < FTSTest
  def create_tables
    # table1 belongs_to table2
    create_table :table1, {
      id: 'SERIAL',
      field1: 'VARCHAR(255)',
      field2: 'TEXT',
      field3: 'TEXT',
      table2_id: 'INT',
    }

    # table2 has_many table1
    create_table :table2, {
      id: 'SERIAL',
      fieldA: 'VARCHAR(255)',
      fieldB: 'TEXT',
      fieldC: 'TEXT',
    }
  end

  def create_fts
    PG::FTS.create { |sql| exec sql }
  end

  def table1_from_table2_fts_index
    @table1_from_table2_fts_index ||= PG::FTS::Index::ManyToOne.new(
      'table2',
      'fieldA',
      'fieldB',
      'fieldC',
      document: 'table1',
    )
  end

  def test_create
    table1_from_table2_fts_index.create { |sql| exec sql }

    expected = %w(
      table1_from_table2_del_tsv
      table1_from_table2_ins_tsv
      table1_from_table2_trn_tsv
      table1_from_table2_upd_tsv
      table2_to_table1_del_tsv
      table2_to_table1_ins_tsv
      table2_to_table1_trn_tsv
      table2_to_table1_upd_tsv
    )

    expected = %w(
      sha1_9769597673bce7a987d0719a06863bc1b24d9a79_src_del_tsv
      sha1_9769597673bce7a987d0719a06863bc1b24d9a79_src_ins_tsv
      sha1_9769597673bce7a987d0719a06863bc1b24d9a79_src_trn_tsv
      sha1_9769597673bce7a987d0719a06863bc1b24d9a79_src_upd_tsv
      sha1_e66173da0059f4f76266a290a02c75f95f958988_doc_del_tsv
      sha1_e66173da0059f4f76266a290a02c75f95f958988_doc_ins_tsv
      sha1_e66173da0059f4f76266a290a02c75f95f958988_doc_trn_tsv
      sha1_e66173da0059f4f76266a290a02c75f95f958988_doc_upd_tsv
    ) if ENV['HASH_NAMES']

    assert_trigger_names(expected)
    assert_procedure_names(expected)
  end

  def test_drop_index
    table1_from_table2_fts_index.create { |sql| exec sql }

    table1_from_table2_fts_index.drop { |sql| exec sql }
    assert_trigger_names([])
    assert_procedure_names([])
  end

  def test_drop_all
    table1_from_table2_fts_index.create { |sql| exec sql }

    PG::FTS::Index.drop_all { |sql| exec sql }
    assert_trigger_names([])
    assert_procedure_names([])
  end

  # scenario relaxed:1.1.1.
  def test_table1_index_on_source_insert_one_related
    exec table1_from_table2_fts_index.on_source_insert_procedure
    exec table1_from_table2_fts_index.on_source_insert_trigger

    insert_into :table1, {
      field1: 'first',
      table2_id: 2,
    }

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:1.1.2.
  def test_table1_index_on_source_insert_many_related
    exec table1_from_table2_fts_index.on_source_insert_procedure
    exec table1_from_table2_fts_index.on_source_insert_trigger

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two),
               %w(alpha bar baz beta foo gamma one three two),
               %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:1.1.3.
  def test_table1_index_on_source_insert_none_related
    exec table1_from_table2_fts_index.on_source_insert_procedure
    exec table1_from_table2_fts_index.on_source_insert_trigger

    insert_into :table1, {
      field1: 'first',
      table2_id: 3,
    }

    insert_into :table1, {
      field1: 'second',
      table2_id: nil,
    }

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    assert_tsv(*[])
  end

  # scenario relaxed:1.2.1. relaxed:1.2.2.
  def test_table1_index_on_source_update
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_source_update_procedure
    exec table1_from_table2_fts_index.on_source_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table2, set: { fieldA: 'seven eight nine' }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta eight foo gamma nine seven),
               %w(alpha bar baz beta eight foo gamma nine seven)

    assert_tsv %w(alpha bar baz beta eight foo gamma nine seven), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta eight foo gamma nine seven), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:1.2.3.
  def test_table1_index_on_source_update_none_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_source_update_procedure
    exec table1_from_table2_fts_index.on_source_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 2,
    }

    update :table2, set: { fieldA: 'seven eight nine' }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:1.2.4.
  def test_table1_index_on_source_update_key
    skip 'unlikely'

    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_source_update_procedure
    exec table1_from_table2_fts_index.on_source_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 3,
    }

    update :table2, set: { id: 4 }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    update :table2, set: { id: 3 }, where: { id: 4 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 3,
      document_table: 'table1',
      document_id: 1,
    }

    update :table2, set: { id: 1 }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:1.3.1.
  def test_table1_index_on_source_delete
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_source_delete_procedure
    exec table1_from_table2_fts_index.on_source_delete_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    delete_from :table2, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.4.
  def test_table1_index_on_source_truncate
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_source_truncate_procedure
    exec table1_from_table2_fts_index.on_source_truncate_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    truncate :table2

    assert_tsv(*[])
  end

  # scenario relaxed:2.1.1.
  def test_table1_index_on_document_insert_one_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 2,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:2.1.2.
  def test_table1_index_on_document_insert_many_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two),
               %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.1.3. relaxed:2.1.4.
  def test_table1_index_on_document_insert_none_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 3,
    }

    assert_tsv(*[])

    insert_into :table1, {
      field1: 'second',
      table2_id: nil,
    }

    assert_tsv(*[])
  end

  # scenario relaxed:2.2.1.
  def test_table1_index_on_document_update
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_update_procedure
    exec table1_from_table2_fts_index.on_document_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table1, set: { table2_id: 2 }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.2.2.1.1
  def test_table1_index_on_document_update_from_null
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_update_procedure
    exec table1_from_table2_fts_index.on_document_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: nil,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table1, set: { table2_id: 1 }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.2.2.1.2
  def test_table1_index_on_document_update_from_null_to_none_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_update_procedure
    exec table1_from_table2_fts_index.on_document_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: nil,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table1, set: { table2_id: 3 }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.2.2.2.1
  def test_table1_index_on_document_update_from_none_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_update_procedure
    exec table1_from_table2_fts_index.on_document_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 3,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table1, set: { table2_id: 1 }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.2.2.2.2
  def test_table1_index_on_document_update_from_none_related_to_none_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_update_procedure
    exec table1_from_table2_fts_index.on_document_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 3,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table1, set: { table2_id: 4 }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.2.2.3
  def test_table1_index_on_document_update_to_null
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_update_procedure
    exec table1_from_table2_fts_index.on_document_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table1, set: { table2_id: nil }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.2.2.4
  def test_table1_index_on_document_update_to_none_related
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_update_procedure
    exec table1_from_table2_fts_index.on_document_update_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    update :table1, set: { table2_id: 3 }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 3,
    }
  end

  # scenario relaxed:2.3.1.
  def test_table1_index_on_document_delete
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_delete_procedure
    exec table1_from_table2_fts_index.on_document_delete_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    delete_from :table1, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table2',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table2',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:2.4.1.
  def test_table1_index_on_document_truncate
    exec table1_from_table2_fts_index.on_document_insert_procedure
    exec table1_from_table2_fts_index.on_document_insert_trigger

    exec table1_from_table2_fts_index.on_document_truncate_procedure
    exec table1_from_table2_fts_index.on_document_truncate_trigger

    insert_into :table2, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
    }

    insert_into :table1, {
      field1: 'first',
      table2_id: 1,
    }, {
      field1: 'second',
      table2_id: 2,
    }, {
      field1: 'third',
      table2_id: 1,
    }

    truncate :table1

    assert_tsv(*[])
  end
end
