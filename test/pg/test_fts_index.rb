require 'minitest/autorun'
require 'helper'
require 'pg/fts'

class TestFTSIndex < FTSTest
  def create_fts
    PG::FTS.create { |sql| exec sql }
    create_index
    fill_data
  end

  def create_tables
    # authors
    create_table :author, {
      id: 'SERIAL',
      name: 'VARCHAR(255)',
    }

    # posts
    create_table :post, {
      id: 'SERIAL',
      title: 'VARCHAR(255)',
      body: 'TEXT',
      author_id: 'INT',
    }

    # comments
    create_table :comment, {
      id: 'SERIAL',
      name: 'VARCHAR(255)',
      body: 'TEXT',
      post_id: 'INT',
    }

    # tags
    create_table :tag, {
      id: 'SERIAL',
      label: 'VARCHAR(255)',
    }

    # post tags
    create_table :post_tag, {
      id: 'SERIAL',
      post_id: 'INT',
      tag_id: 'INT',
    }
  end

  module CommentIndex
    extend PG::FTS::Index::Module

    SelfIndex   = PG::FTS::Index::Self.new('comment', 'body')
    PostIndex   = PG::FTS::Index::ManyToOne.new('post', 'title',
                                                document: 'comment')
    AuthorIndex = PG::FTS::Index::MOMO.new('author',
                                           'name',
                                           document: 'comment',
                                           link: 'post')
  end

  module PostIndex
    extend PG::FTS::Index::Module

    SelfIndex   = PG::FTS::Index::Self.new('post', 'body', 'title')
    AuthorIndex = PG::FTS::Index::ManyToOne.new('author',
                                                'name',
                                                document: 'post')
    TagIndex    = PG::FTS::Index::OMMO.new('tag',
                                           'label',
                                           document: 'post',
                                           link: 'post_tag')
  end

  module AuthorIndex
    extend PG::FTS::Index::Module

    SelfIndex = PG::FTS::Index::Self.new('author', 'name')
    PostIndex = PG::FTS::Index::OneToMany.new('post',
                                              'title',
                                              'body',
                                              document: 'author')
  end

  def indices
    [
      CommentIndex,
      PostIndex,
      AuthorIndex,
    ]
  end

  def create_index
    PG::FTS::Index.create(*indices) { |sql| exec sql }
  end

  def fill_data
    insert_into :author,
                { id: 1, name: 'Fred Foowriter' },
                { id: 2, name: 'Tom Bartender'  },
                { id: 3, name: 'Kim Bazinger'   }

    insert_into :tag,
                { id:  1, label: 'ecstatic'     },
                { id:  2, label: 'sarcastic'    },
                { id:  3, label: 'emphatic'     },
                { id:  4, label: 'acrobatic'    },
                { id:  5, label: 'aneurysmatic' },
                { id:  6, label: 'enigmatic'    },
                { id:  7, label: 'idiomatic'    },
                { id:  8, label: 'lunatic'      },
                { id:  9, label: 'overdramatic' },
                { id: 10, label: 'schismatic'   },
                { id: 11, label: 'tropismatic'  },
                { id: 12, label: 'unschematic'  },
                { id: 13, label: 'zeugmatic'    }

    insert_into :post, {
      id: 1,
      title: 'Ten thousand ways to lose your mind',
      body: 'There is more Unix-nature in one line of shell script ' \
            'than there is in ten thousand lines of C',
      author_id: 1,
    }, {
      id: 2,
      title: 'The Programming People',
      body: 'Even the hacker who works alone collaborates with others, ' \
            'and must constantly communicate clearly to them, lest his ' \
            'work become confused and lost.',
      author_id: 1,
    }, {
      id: 3,
      title: 'Shake it up, high five, ten up!',
      body: 'Shaken, not stirred',
      author_id: 2,
    }, {
      id: 4,
      title: 'Five ways to impossibru!!!!!',
      body: 'In a digital ocean of possibilities, the memes are endless.',
      author_id: 2,
    }, {
      id: 5,
      title: 'Chocolate Sky',
      body: 'Lost in vacation, not a translation.',
      author_id: 2,
    }, {
      id: 6,
      title: 'NaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaN',
      body: 'Batman! WAT the memes!',
      author_id: 3,
    }

    insert_into :post_tag, {
      post_id: 1,
      tag_id: 2,
    }, {
      post_id: 1,
      tag_id: 4,
    }, {
      post_id: 2,
      tag_id: 4,
    }, {
      post_id: 3,
      tag_id: 7,
    }, {
      post_id: 4,
      tag_id: 3,
    }, {
      post_id: 4,
      tag_id: 13,
    }, {
      post_id: 4,
      tag_id: 7,
    }, {
      post_id: 5,
      tag_id: 2,
    }, {
      post_id: 5,
      tag_id: 12,
    }, {
      post_id: 6,
      tag_id: 8,
    }, {
      post_id: 6,
      tag_id: 2,
    }, {
      post_id: 6,
      tag_id: 9,
    }, {
      post_id: 6,
      tag_id: 13,
    }

    insert_into :comment, {
      id: 1,
      post_id: 1,
      name: 'Anonymous coward',
      body: '',
    }, {
      id: 2,
      post_id: 1,
      name: 'Thoughtless bastard',
      body: '',
    }, {
      id: 3,
      post_id: 3,
      name: 'Freaky retard',
      body: '',
    }, {
      id: 4,
      post_id: 3,
      name: 'Mindless bastard',
      body: '',
    }, {
      id: 5,
      post_id: 3,
      name: 'Ginormous coward',
      body: '',
    }, {
      id: 6,
      post_id: 4,
      name: 'Brainless bastard',
      body: '',
    }, {
      id: 7,
      post_id: 4,
      name: 'Frivolous coward',
      body: '',
    }, {
      id: 8,
      post_id: 5,
      name: 'Senseless bastard',
      body: '',
    }, {
      id: 9,
      post_id: 6,
      name: 'Pesky retard',
      body: '',
    }
  end

  def test_index_created
    expected = %w(
      author_from_author_del_tsv
      author_from_author_ins_tsv
      author_from_author_trn_tsv
      author_from_author_upd_tsv
      author_from_post_del_tsv
      author_from_post_ins_tsv
      author_from_post_trn_tsv
      author_from_post_upd_tsv
      author_to_comment_del_tsv
      author_to_comment_ins_tsv
      author_to_comment_trn_tsv
      author_to_comment_upd_tsv
      author_to_post_del_tsv
      author_to_post_ins_tsv
      author_to_post_trn_tsv
      author_to_post_upd_tsv
      comment_from_author_del_tsv
      comment_from_author_ins_tsv
      comment_from_author_trn_tsv
      comment_from_author_upd_tsv
      comment_from_comment_del_tsv
      comment_from_comment_ins_tsv
      comment_from_comment_trn_tsv
      comment_from_comment_upd_tsv
      comment_from_post_del_tsv
      comment_from_post_ins_tsv
      comment_from_post_trn_tsv
      comment_from_post_upd_tsv
      post_from_author_del_tsv
      post_from_author_ins_tsv
      post_from_author_trn_tsv
      post_from_author_upd_tsv
      post_from_post_del_tsv
      post_from_post_ins_tsv
      post_from_post_trn_tsv
      post_from_post_upd_tsv
      post_from_tag_del_tsv
      post_from_tag_ins_tsv
      post_from_tag_trn_tsv
      post_from_tag_upd_tsv
      post_tag_to_post_from_tag_del_tsv
      post_tag_to_post_from_tag_ins_tsv
      post_tag_to_post_from_tag_trn_tsv
      post_tag_to_post_from_tag_upd_tsv
      post_to_author_del_tsv
      post_to_author_ins_tsv
      post_to_author_trn_tsv
      post_to_author_upd_tsv
      post_to_comment_del_tsv
      post_to_comment_from_author_del_tsv
      post_to_comment_from_author_ins_tsv
      post_to_comment_from_author_trn_tsv
      post_to_comment_from_author_upd_tsv
      post_to_comment_ins_tsv
      post_to_comment_trn_tsv
      post_to_comment_upd_tsv
      tag_to_post_del_tsv
      tag_to_post_ins_tsv
      tag_to_post_trn_tsv
      tag_to_post_upd_tsv
    )

    expected = %w(
      sha1_09186144d61c5b15f256fe19a4ab02e7b838e5b8_doc_del_tsv
      sha1_09186144d61c5b15f256fe19a4ab02e7b838e5b8_doc_ins_tsv
      sha1_09186144d61c5b15f256fe19a4ab02e7b838e5b8_doc_trn_tsv
      sha1_09186144d61c5b15f256fe19a4ab02e7b838e5b8_doc_upd_tsv
      sha1_09ab55069c0d7fe947a280febafafb0ca9cc7252_src_del_tsv
      sha1_09ab55069c0d7fe947a280febafafb0ca9cc7252_src_ins_tsv
      sha1_09ab55069c0d7fe947a280febafafb0ca9cc7252_src_trn_tsv
      sha1_09ab55069c0d7fe947a280febafafb0ca9cc7252_src_upd_tsv
      sha1_1226a5ab0376ee3bb40f43bc76bd56f9f36d9afb_src_del_tsv
      sha1_1226a5ab0376ee3bb40f43bc76bd56f9f36d9afb_src_ins_tsv
      sha1_1226a5ab0376ee3bb40f43bc76bd56f9f36d9afb_src_trn_tsv
      sha1_1226a5ab0376ee3bb40f43bc76bd56f9f36d9afb_src_upd_tsv
      sha1_297d1e81b2e520c7e68e5bd5191a8d12134ca497_doc_del_tsv
      sha1_297d1e81b2e520c7e68e5bd5191a8d12134ca497_doc_ins_tsv
      sha1_297d1e81b2e520c7e68e5bd5191a8d12134ca497_doc_trn_tsv
      sha1_297d1e81b2e520c7e68e5bd5191a8d12134ca497_doc_upd_tsv
      sha1_352adbe42c4b091578a971040cd9cbc19f322d55_lnk_del_tsv
      sha1_352adbe42c4b091578a971040cd9cbc19f322d55_lnk_ins_tsv
      sha1_352adbe42c4b091578a971040cd9cbc19f322d55_lnk_trn_tsv
      sha1_352adbe42c4b091578a971040cd9cbc19f322d55_lnk_upd_tsv
      sha1_4a5ce86fbf18a178efe35ab50db0b319792bad67_doc_del_tsv
      sha1_4a5ce86fbf18a178efe35ab50db0b319792bad67_doc_ins_tsv
      sha1_4a5ce86fbf18a178efe35ab50db0b319792bad67_doc_trn_tsv
      sha1_4a5ce86fbf18a178efe35ab50db0b319792bad67_doc_upd_tsv
      sha1_4a6cc1a514bfdb71d20bb6921d59aa255c5a9d1e_src_del_tsv
      sha1_4a6cc1a514bfdb71d20bb6921d59aa255c5a9d1e_src_ins_tsv
      sha1_4a6cc1a514bfdb71d20bb6921d59aa255c5a9d1e_src_trn_tsv
      sha1_4a6cc1a514bfdb71d20bb6921d59aa255c5a9d1e_src_upd_tsv
      sha1_8cdf20be1516ac4a9e9d98c4e0421d872f4ecd2e_doc_del_tsv
      sha1_8cdf20be1516ac4a9e9d98c4e0421d872f4ecd2e_doc_ins_tsv
      sha1_8cdf20be1516ac4a9e9d98c4e0421d872f4ecd2e_doc_trn_tsv
      sha1_8cdf20be1516ac4a9e9d98c4e0421d872f4ecd2e_doc_upd_tsv
      sha1_9aa49e745c8b957c4538539dadd7c22bfd3096fd_src_del_tsv
      sha1_9aa49e745c8b957c4538539dadd7c22bfd3096fd_src_ins_tsv
      sha1_9aa49e745c8b957c4538539dadd7c22bfd3096fd_src_trn_tsv
      sha1_9aa49e745c8b957c4538539dadd7c22bfd3096fd_src_upd_tsv
      sha1_ad8955d68c369ea5ae651a5cbf16113b518735a9_doc_del_tsv
      sha1_ad8955d68c369ea5ae651a5cbf16113b518735a9_doc_ins_tsv
      sha1_ad8955d68c369ea5ae651a5cbf16113b518735a9_doc_trn_tsv
      sha1_ad8955d68c369ea5ae651a5cbf16113b518735a9_doc_upd_tsv
      sha1_b86cfda8ec9d591e3536252880b5f870dde0a8e7_doc_del_tsv
      sha1_b86cfda8ec9d591e3536252880b5f870dde0a8e7_doc_ins_tsv
      sha1_b86cfda8ec9d591e3536252880b5f870dde0a8e7_doc_trn_tsv
      sha1_b86cfda8ec9d591e3536252880b5f870dde0a8e7_doc_upd_tsv
      sha1_d505d65e92268f5c2ff7029e44a8f61bdabb2789_doc_del_tsv
      sha1_d505d65e92268f5c2ff7029e44a8f61bdabb2789_doc_ins_tsv
      sha1_d505d65e92268f5c2ff7029e44a8f61bdabb2789_doc_trn_tsv
      sha1_d505d65e92268f5c2ff7029e44a8f61bdabb2789_doc_upd_tsv
      sha1_d922c7a83140e3e937d49100dcdd7076fe8ad49b_lnk_del_tsv
      sha1_d922c7a83140e3e937d49100dcdd7076fe8ad49b_lnk_ins_tsv
      sha1_d922c7a83140e3e937d49100dcdd7076fe8ad49b_lnk_trn_tsv
      sha1_d922c7a83140e3e937d49100dcdd7076fe8ad49b_lnk_upd_tsv
      sha1_e7bd253c1435c4bb5fa9b5d03faf7e5a397f723f_doc_del_tsv
      sha1_e7bd253c1435c4bb5fa9b5d03faf7e5a397f723f_doc_ins_tsv
      sha1_e7bd253c1435c4bb5fa9b5d03faf7e5a397f723f_doc_trn_tsv
      sha1_e7bd253c1435c4bb5fa9b5d03faf7e5a397f723f_doc_upd_tsv
      sha1_fc2de6da2673b24474929546a96679a89d52c35f_src_del_tsv
      sha1_fc2de6da2673b24474929546a96679a89d52c35f_src_ins_tsv
      sha1_fc2de6da2673b24474929546a96679a89d52c35f_src_trn_tsv
      sha1_fc2de6da2673b24474929546a96679a89d52c35f_src_upd_tsv
    ) if ENV['HASH_NAMES']

    assert_trigger_names(expected)
    assert_procedure_names(expected)
  end

  def test_index_populated
    assert_count(61, select(count('*'), from: :fts))
    assert_count(9, select(count('*'),
                           from: :fts,
                           where: { document_table: 'author' }))
    assert_count(25, select(count('*'),
                            from: :fts,
                            where: { document_table: 'post' }))
    assert_count(27, select(count('*'),
                            from: :fts,
                            where: { document_table: 'comment' }))
  end

  def test_clear
    PG::FTS::Index.clear(*indices) { |sql| exec sql }
    assert_tsv(*[])
  end

  def test_clear_author
    PG::FTS::Index.clear(AuthorIndex) { |sql| exec sql }
    assert_tsv(*[], where: { document_table: 'author' })
    assert_count(25, select(count('*'),
                            from: :fts,
                            where: { document_table: 'post' }))
    assert_count(27, select(count('*'),
                            from: :fts,
                            where: { document_table: 'comment' }))
  end

  def test_clear_post
    PG::FTS::Index.clear(PostIndex) { |sql| exec sql }
    assert_tsv(*[], where: { document_table: 'post' })
    assert_count(9, select(count('*'),
                           from: :fts,
                           where: { document_table: 'author' }))
    assert_count(27, select(count('*'),
                            from: :fts,
                            where: { document_table: 'comment' }))
  end

  def test_clear_comment
    PG::FTS::Index.clear(CommentIndex) { |sql| exec sql }
    assert_tsv(*[], where: { document_table: 'comment' })
    assert_count(9, select(count('*'),
                           from: :fts,
                           where: { document_table: 'author' }))
    assert_count(25, select(count('*'),
                            from: :fts,
                            where: { document_table: 'post' }))
  end

  def test_build_author
    assert_stable_fts do
      PG::FTS::Index.clear(AuthorIndex) { |sql| exec sql }
      PG::FTS::Index.build(AuthorIndex) { |sql| exec sql }
    end
  end

  def test_build_post
    assert_stable_fts do
      PG::FTS::Index.clear(PostIndex) { |sql| exec sql }
      PG::FTS::Index.build(PostIndex) { |sql| exec sql }
    end
  end

  def test_build_comment
    assert_stable_fts do
      PG::FTS::Index.clear(CommentIndex) { |sql| exec sql }
      PG::FTS::Index.build(CommentIndex) { |sql| exec sql }
    end
  end

  def test_build
    assert_stable_fts do
      PG::FTS::Index.clear(*indices) { |sql| exec sql }
      PG::FTS::Index.build(*indices) { |sql| exec sql }
    end
  end
end
