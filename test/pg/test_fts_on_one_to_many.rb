require 'minitest/autorun'
require 'helper'
require 'pg/fts'

class TestFTSOnOneToMany < FTSTest
  def create_tables
    # table1 has_many table3
    create_table :table1, {
      id: 'SERIAL',
      field1: 'VARCHAR(255)',
      field2: 'TEXT',
      field3: 'TEXT',
    }

    # table3 belongs_to table1
    create_table :table3, {
      id: 'SERIAL',
      fieldA: 'VARCHAR(255)',
      fieldB: 'TEXT',
      fieldC: 'TEXT',
      table1_id: 'INT',
    }
  end

  def create_fts
    PG::FTS.create { |sql| exec sql }
  end

  def table1_from_table3_fts_index
    @table1_from_table3_fts_index ||= PG::FTS::Index::OneToMany.new(
      'table3',
      'fieldA',
      'fieldB',
      'fieldC',
      document: 'table1',
    )
  end

  def test_create
    table1_from_table3_fts_index.create { |sql| exec sql }

    expected = %w(
      table1_from_table3_del_tsv
      table1_from_table3_ins_tsv
      table1_from_table3_trn_tsv
      table1_from_table3_upd_tsv
      table3_to_table1_del_tsv
      table3_to_table1_ins_tsv
      table3_to_table1_trn_tsv
      table3_to_table1_upd_tsv
    )

    expected = %w(
      sha1_0b43c8c98ede5f8bdcf6121d1ebb1a38a6142707_src_del_tsv
      sha1_0b43c8c98ede5f8bdcf6121d1ebb1a38a6142707_src_ins_tsv
      sha1_0b43c8c98ede5f8bdcf6121d1ebb1a38a6142707_src_trn_tsv
      sha1_0b43c8c98ede5f8bdcf6121d1ebb1a38a6142707_src_upd_tsv
      sha1_6bd6677027107801bc3b4af4f85c265061f7b589_doc_del_tsv
      sha1_6bd6677027107801bc3b4af4f85c265061f7b589_doc_ins_tsv
      sha1_6bd6677027107801bc3b4af4f85c265061f7b589_doc_trn_tsv
      sha1_6bd6677027107801bc3b4af4f85c265061f7b589_doc_upd_tsv
    ) if ENV['HASH_NAMES']

    assert_trigger_names(expected)
    assert_procedure_names(expected)
  end

  def test_drop_index
    table1_from_table3_fts_index.create { |sql| exec sql }

    table1_from_table3_fts_index.drop { |sql| exec sql }
    assert_trigger_names([])
    assert_procedure_names([])
  end

  def test_drop_all
    table1_from_table3_fts_index.create { |sql| exec sql }

    PG::FTS::Index.drop_all { |sql| exec sql }
    assert_trigger_names([])
    assert_procedure_names([])
  end

  # scenario relaxed:1.1.1.
  def test_table1_index_on_source_insert_one_related
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.1.2
  def test_table1_index_on_source_insert_many_related
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }, {
      field1: 'third',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two),
               %w(eight epsilon iota papp pippo pluto seven six theta)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta), where: {
      source_table: 'table3',
      source_id: 3,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:1.1.3. relaxed:1.1.4.
  def test_table1_index_on_source_insert_none_related
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: nil,
    }

    assert_tsv(*[])

    insert_into :table3, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 3,
    }

    assert_tsv(*[])
  end

  # scenario relaxed:1.2.1
  def test_table1_index_on_source_update
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    update :table3, set: { fieldA: 'seven eight nine' }, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta eight foo gamma nine seven),
               %w(eight epsilon iota papp pippo pluto seven six theta)

    assert_tsv %w(alpha bar baz beta eight foo gamma nine seven), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta), where: {
      source_table: 'table3',
      source_id: 3,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:1.2.2
  def test_table1_index_on_source_update_relation
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    update :table3, set: { table1_id: 2 }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two),
               %w(eight epsilon iota papp pippo pluto seven six theta)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta), where: {
      source_table: 'table3',
      source_id: 3,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.2.3.1
  def test_table1_index_on_source_update_relation_from_null
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: nil,
    }

    update :table3, set: { table1_id: 2 }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two),
               %w(eight epsilon iota papp pippo pluto seven six theta)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta), where: {
      source_table: 'table3',
      source_id: 3,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.2.3.2
  def test_table1_index_on_source_update_relation_from_null_to_none_related
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: nil,
    }

    update :table3, set: { table1_id: 3 }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.2.4.1
  def test_table1_index_on_source_update_relation_from_none_related
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 3,
    }

    update :table3, set: { table1_id: 2 }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two),
               %w(eight epsilon iota papp pippo pluto seven six theta)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta), where: {
      source_table: 'table3',
      source_id: 3,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.2.4.2
  def test_table1_index_on_source_update_relation_from_none_related_to_null
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 3,
    }

    update :table3, set: { table1_id: nil }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    update :table3, set: { table1_id: 5 }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.2.5.1
  def test_table1_index_on_source_update_relation_to_null
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    update :table3, set: { table1_id: nil }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.2.6.1
  def test_table1_index_on_source_update_relation_to_none_related
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_update_procedure
    exec table1_from_table3_fts_index.on_source_update_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    update :table3, set: { table1_id: 4 }, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.3.
  def test_table1_index_on_source_delete
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_delete_procedure
    exec table1_from_table3_fts_index.on_source_delete_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    delete_from :table3, where: { id: 3 }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:1.4.
  def test_table1_index_on_source_truncate
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_source_truncate_procedure
    exec table1_from_table3_fts_index.on_source_truncate_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    truncate :table3

    assert_tsv(*[])
  end

  # scenario relaxed:2.1.1
  def test_table1_index_on_document_insert
    exec table1_from_table3_fts_index.on_document_insert_procedure
    exec table1_from_table3_fts_index.on_document_insert_trigger

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }, {
      field1: 'third',
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta),
               %w(alpha bar baz beta foo gamma one three two),
               %w(eight epsilon iota papp pippo pluto seven six theta)

    assert_tsv %w(alpha bar baz beta foo gamma one three two), where: {
      source_table: 'table3',
      source_id: 1,
      document_table: 'table1',
      document_id: 1,
    }

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta), where: {
      source_table: 'table3',
      source_id: 3,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:2.1.2
  def test_table1_index_on_document_insert_none_related
    exec table1_from_table3_fts_index.on_document_insert_procedure
    exec table1_from_table3_fts_index.on_document_insert_trigger

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 4,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 5,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }, {
      field1: 'third',
    }

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta)

    assert_tsv %w(eight epsilon iota papp pippo pluto seven six theta), where: {
      source_table: 'table3',
      source_id: 3,
      document_table: 'table1',
      document_id: 1,
    }
  end

  # scenario relaxed:2.2.
  def test_table1_index_on_document_update
    skip 'nothing'
  end

  # scenario relaxed:2.3.
  def test_table1_index_on_document_delete
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_document_delete_procedure
    exec table1_from_table3_fts_index.on_document_delete_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    delete_from :table1, where: { id: 1 }

    assert_tsv %w(alice bob delta eta eve five four three zeta)

    assert_tsv %w(alice bob delta eta eve five four three zeta), where: {
      source_table: 'table3',
      source_id: 2,
      document_table: 'table1',
      document_id: 2,
    }
  end

  # scenario relaxed:2.4.
  def test_table1_index_on_document_truncate
    exec table1_from_table3_fts_index.on_source_insert_procedure
    exec table1_from_table3_fts_index.on_source_insert_trigger

    exec table1_from_table3_fts_index.on_document_truncate_procedure
    exec table1_from_table3_fts_index.on_document_truncate_trigger

    insert_into :table1, {
      field1: 'first',
    }, {
      field1: 'second',
    }

    insert_into :table3, {
      fieldA: 'one two three',
      fieldB: 'foo bar baz',
      fieldC: 'alpha beta gamma',
      table1_id: 1,
    }, {
      fieldA: 'three four five',
      fieldB: 'alice bob eve',
      fieldC: 'delta eta zeta',
      table1_id: 2,
    }, {
      fieldA: 'six seven eight',
      fieldB: 'pippo pluto papp',
      fieldC: 'epsilon theta iota',
      table1_id: 1,
    }

    truncate :table1

    assert_tsv(*[])
  end
end
