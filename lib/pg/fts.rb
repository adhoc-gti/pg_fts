module PG::FTS
  module_function

  def table
    'fts'
  end

  def catalog
    'pg_catalog.simple'
  end

  def create_table_query
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TABLE "#{PG::FTS.table}" (
      "id" SERIAL,
      "document_table" VARCHAR(255),
      "document_id" INT,
      "source_table" VARCHAR(255),
      "source_id" INT,
      "tsv" TSVECTOR);
    SQL
  end

  def create_key_index_query
    <<-SQL.gsub(/^ {4}/, '')
    CREATE UNIQUE INDEX "#{PG::FTS.table}_idx"
    ON "#{PG::FTS.table}" (
      "document_table",
      "document_id",
      "source_table",
      "source_id");
    SQL
  end

  def create_tsv_index_query
    <<-SQL.gsub(/^ {4}/, '')
    CREATE INDEX "#{PG::FTS.table}_tsv_idx"
    ON "#{PG::FTS.table}"
    USING GIN ("tsv");
    SQL
  end

  def drop_table_query
    <<-SQL.gsub(/^ {4}/, '')
    DROP TABLE "#{PG::FTS.table}"
    SQL
  end

  def truncate_table_query
    <<-SQL.gsub(/^ {4}/, '')
    TRUNCATE "#{PG::FTS.table}";
    SQL
  end

  def create
    yield(create_table_query)
    yield(create_key_index_query)
    yield(create_tsv_index_query)
  end

  def drop
    yield(drop_table_query)
  end

  def clear
    yield(truncate_table_query)
  end
end

require 'pg/fts/index'
