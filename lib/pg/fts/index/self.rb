class PG::FTS::Index::Self
  attr_reader :table, :fields, :key, :catalog, :name
  alias source table
  alias document table
  alias foreign_key key

  include PG::FTS::Index
  include PG::FTS::Naming
  include PG::FTS::TSVector

  def initialize(table, *fields, key: nil, catalog: nil, as: nil)
    @table = table
    @fields = fields
    @key = key || :id
    @catalog = catalog || PG::FTS.catalog
    @name = as
  end

  def build_query
    <<-SQL.gsub(/^ {4}/, '')
    INSERT INTO "#{PG::FTS.table}" (
      "document_id",
      "document_table",
      "source_id",
      "source_table",
      "tsv")
    SELECT "#{document}"."#{key}",
           '#{document}',
           "#{source}"."#{key}",
           '#{source}',
           #{ts_vector(source)}
    FROM "#{document}";
    SQL
  end

  def on_document_insert_query
    <<-SQL.gsub(/^ {4}/, '')
    INSERT INTO "#{PG::FTS.table}" (
      "document_id",
      "document_table",
      "source_id",
      "source_table",
      "tsv")
    VALUES (
      NEW."#{key}",
      '#{document}',
      NEW."#{key}",
      '#{source}',
      #{ts_vector});
    SQL
  end

  def on_document_insert_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_insert_procedure_name}"() RETURNS TRIGGER AS $$
      BEGIN
        #{on_document_insert_query.gsub(/^/, '    ')}
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_update_query
    <<-SQL.gsub(/^ {4}/, '')
    UPDATE "#{PG::FTS.table}" SET "tsv" = (#{ts_vector})
    WHERE "#{PG::FTS.table}"."source_id" = NEW."#{key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_id" = NEW."#{key}"
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_document_update_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_update_procedure_name}"() RETURNS TRIGGER AS $$
      BEGIN
        #{on_document_update_query.gsub(/^/, '    ')}
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_delete_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
    WHERE "#{PG::FTS.table}"."source_id" = OLD."#{key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_id" = OLD."#{key}"
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_document_delete_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_delete_procedure_name}"() RETURNS TRIGGER AS $$
      BEGIN
        #{on_document_delete_query.gsub(/^/, '    ')}
        RETURN OLD;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_truncate_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
    WHERE "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_document_truncate_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_truncate_procedure_name}"() RETURNS TRIGGER AS $$
      BEGIN
        #{on_document_truncate_query.gsub(/^/, '    ')}
        RETURN NULL;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_insert_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_insert_trigger_name}"
    AFTER INSERT ON "#{document}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_document_insert_procedure_name}"();
    SQL
  end

  def on_document_update_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_update_trigger_name}"
    AFTER UPDATE ON "#{document}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_document_update_procedure_name}"();
    SQL
  end

  def on_document_delete_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_delete_trigger_name}"
    AFTER DELETE ON "#{document}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_document_delete_procedure_name}"();
    SQL
  end

  def on_document_truncate_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_truncate_trigger_name}"
    AFTER TRUNCATE ON "#{document}"
    FOR EACH STATEMENT
    EXECUTE PROCEDURE "#{on_document_truncate_procedure_name}"();
    SQL
  end
end
