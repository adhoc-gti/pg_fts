# many documents, one link, many links, one source
class PG::FTS::Index::MOMO
  attr_reader :table, :fields, :catalog,
              :document, :link,
              :key, :foreign_key, :forlink_key,
              :name
  alias source table

  include PG::FTS::Index
  include PG::FTS::Naming
  include PG::FTS::TSVector

  # rubocop:disable Metrics/ParameterLists, Metrics/CyclomaticComplexity
  def initialize(table, *fields,
                 document: nil, link: nil,
                 key: nil, foreign_key: nil, forlink_key: nil,
                 catalog: nil,
                 as: nil)
    @table = table
    @fields = fields
    @catalog = catalog || PG::FTS.catalog
    @document = document || fail(ArgumentError, ':document required')
    @link = link || fail(ArgumentError, ':link required')
    @key = key || :id
    @foreign_key = foreign_key || :"#{table}_id"
    @forlink_key = forlink_key || :"#{link}_id"
    @name = as
  end
  # rubocop:enable Metrics/ParameterLists, Metrics/CyclomaticComplexity

  def build_query
    <<-SQL.gsub(/^ {4}/, '')
    INSERT INTO "#{PG::FTS.table}" (
      "document_id",
      "document_table",
      "source_id",
      "source_table",
      "tsv")
    SELECT "#{document}"."#{key}",
           '#{document}',
           "#{source}"."#{key}",
           '#{source}',
           #{ts_vector(source)}
    FROM "#{document}", "#{source}", "#{link}"
    WHERE "#{document}"."#{forlink_key}" = "#{link}"."#{key}"
      AND "#{link}"."#{foreign_key}" = "#{source}"."#{key}";
    SQL
  end

  def on_source_insert_query
    <<-SQL.gsub(/^ {4}/, '')
    INSERT INTO "#{PG::FTS.table}" (
      "document_id",
      "document_table",
      "source_id",
      "source_table",
      "tsv")
    SELECT
      "#{document}"."#{key}",
      '#{document}',
      NEW."#{key}",
      '#{source}',
      #{ts_vector}
    FROM "#{document}", "#{link}"
    WHERE "#{document}"."#{forlink_key}" = "#{link}"."#{key}"
      AND "#{link}"."#{foreign_key}" = NEW."#{key}";
    SQL
  end

  def on_source_insert_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_source_insert_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_source_insert_query.gsub(/^/, '    ')}
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_source_update_query
    <<-SQL.gsub(/^ {4}/, '')
    UPDATE "#{PG::FTS.table}"
    SET
      "tsv" = (#{ts_vector}),
      "document_table" = '#{document}',
      "document_id" = "#{document}"."#{key}"
    FROM "#{document}", "#{link}"
    WHERE "#{document}"."#{forlink_key}" = "#{link}"."#{key}"
      AND "#{link}"."#{foreign_key}" = NEW."#{key}"
      AND "#{PG::FTS.table}"."source_id" = OLD."#{key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_id" = "#{document}"."#{key}"
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_source_update_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_source_update_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_source_update_query.gsub(/^/, '    ')}
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_source_delete_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
    WHERE "#{PG::FTS.table}"."source_id" = OLD."#{key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_source_delete_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_source_delete_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_source_delete_query.gsub(/^/, '    ')}
        RETURN OLD;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_source_truncate_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
    WHERE "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_source_truncate_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_source_truncate_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_source_truncate_query.gsub(/^/, '    ')}
        RETURN NULL;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_source_insert_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_source_insert_trigger_name}"
    AFTER INSERT ON "#{source}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_source_insert_procedure_name}"();
    SQL
  end

  def on_source_update_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_source_update_trigger_name}"
    AFTER UPDATE ON "#{source}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_source_update_procedure_name}"();
    SQL
  end

  def on_source_delete_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_source_delete_trigger_name}"
    AFTER DELETE ON "#{source}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_source_delete_procedure_name}"();
    SQL
  end

  def on_source_truncate_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_source_truncate_trigger_name}"
    AFTER TRUNCATE ON "#{source}"
    FOR EACH STATEMENT
    EXECUTE PROCEDURE "#{on_source_truncate_procedure_name}"();
    SQL
  end

  def on_document_insert_query
    <<-SQL.gsub(/^ {4}/, '')
    INSERT INTO "#{PG::FTS.table}" (
      "document_id",
      "document_table",
      "source_id",
      "source_table",
      "tsv")
    SELECT
      NEW."#{key}",
      '#{document}',
      "#{source}"."#{key}",
      '#{source}',
      #{ts_vector(source)}
    FROM "#{source}", "#{link}"
    WHERE "#{source}"."#{key}" = "#{link}"."#{foreign_key}"
      AND "#{link}"."#{key}" = NEW."#{forlink_key}";
    SQL
  end

  def on_document_insert_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_insert_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        IF NEW."#{forlink_key}" IS NULL THEN
          RETURN NEW;
        END IF;
        #{on_document_insert_query.gsub(/^/, '    ')}
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_update_query
    <<-SQL.gsub(/^ {4}/, '')
    UPDATE "#{PG::FTS.table}"
    SET
      "tsv" = (#{ts_vector(source)}),
      "source_table" = '#{source}',
      "source_id" = "#{source}"."#{key}"
    FROM "#{source}" AS "OLD_#{source}", "#{source}",
          "#{link}" AS "OLD_#{link}", "#{link}"
    WHERE "OLD_#{source}"."#{key}" = "OLD_#{link}"."#{foreign_key}"
      AND "OLD_#{link}"."#{key}" = OLD."#{forlink_key}"
      AND "#{source}"."#{key}" = "#{link}"."#{foreign_key}"
      AND "#{link}"."#{key}" = NEW."#{forlink_key}"
      AND "#{PG::FTS.table}"."source_id" = "OLD_#{link}"."#{foreign_key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_id" = OLD."#{key}"
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_document_update_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_update_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        IF NEW."#{forlink_key}" IS NULL AND OLD."#{forlink_key}" IS NULL THEN
          RETURN NEW;
        END IF;
        IF NEW."#{forlink_key}" IS NULL THEN
          #{on_document_delete_query.gsub(/^/, '      ')}
          RETURN NEW;
        END IF;
        IF OLD."#{forlink_key}" IS NULL THEN
          #{on_document_insert_query.gsub(/^/, '      ')}
          RETURN NEW;
        END IF;
        #{on_document_update_query.gsub(/^/, '    ')}
        IF NOT FOUND THEN
            #{on_document_insert_query.gsub(/^/, '      ')}
          IF NOT FOUND THEN
            #{on_document_delete_query.gsub(/^/, '      ')}
          END IF;
          RETURN NEW;
        END IF;
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_delete_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
          USING "#{link}"
    WHERE "#{link}"."#{key}" = OLD."#{forlink_key}"
      AND "#{PG::FTS.table}"."source_id" = "#{link}"."#{foreign_key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_id" = OLD."#{key}"
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_document_delete_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_delete_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_document_delete_query.gsub(/^/, '    ')}
        RETURN OLD;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_truncate_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
    WHERE "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_document_truncate_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_document_truncate_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_document_truncate_query.gsub(/^/, '    ')}
        RETURN NULL;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_document_insert_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_insert_trigger_name}"
    AFTER INSERT ON "#{document}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_document_insert_procedure_name}"();
    SQL
  end

  def on_document_update_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_update_trigger_name}"
    AFTER UPDATE ON "#{document}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_document_update_procedure_name}"();
    SQL
  end

  def on_document_delete_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_delete_trigger_name}"
    AFTER DELETE ON "#{document}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_document_delete_procedure_name}"();
    SQL
  end

  def on_document_truncate_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_document_truncate_trigger_name}"
    AFTER TRUNCATE ON "#{document}"
    FOR EACH STATEMENT
    EXECUTE PROCEDURE "#{on_document_truncate_procedure_name}"();
    SQL
  end

  def on_link_insert_query
    <<-SQL.gsub(/^ {4}/, '')
    INSERT INTO "#{PG::FTS.table}" (
      "document_id",
      "document_table",
      "source_id",
      "source_table",
      "tsv")
    SELECT
      "#{document}"."#{key}",
      '#{document}',
      "#{source}"."#{key}",
      '#{source}',
      #{ts_vector(source)}
    FROM "#{document}", "#{source}"
    WHERE "#{document}"."#{forlink_key}" = NEW."#{key}"
      AND "#{source}"."#{key}" = NEW."#{foreign_key}";
    SQL
  end

  def on_link_insert_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_link_insert_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_link_insert_query.gsub(/^/, '    ')}
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_link_update_query
    <<-SQL.gsub(/^ {4}/, '')
    UPDATE "#{PG::FTS.table}"
    SET
      "tsv" = (#{ts_vector(source)}),
      "document_table" = '#{document}',
      "document_id" = "#{document}"."#{key}",
      "source_table" = '#{source}',
      "source_id" = NEW."#{foreign_key}"
    FROM "#{document}", "#{link}", "#{source}"
    WHERE "#{document}"."#{forlink_key}" = OLD."#{key}"
      AND NEW."#{foreign_key}" = "#{source}"."#{key}"
      AND "#{PG::FTS.table}"."source_id" = OLD."#{foreign_key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_id" = "#{document}"."#{key}"
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_link_update_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_link_update_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_link_update_query.gsub(/^/, '    ')}
        IF NOT FOUND THEN
          #{on_link_insert_query.gsub(/^/, '      ')}
          IF NOT FOUND THEN
            #{on_link_delete_query.gsub(/^/, '        ')}
          END IF;
          RETURN NEW;
        END IF;
        RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_link_delete_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
          USING "#{link}", "#{document}", "#{source}"
    WHERE OLD."#{key}" = "#{document}"."#{forlink_key}"
      AND OLD."#{foreign_key}" = "#{source}"."#{key}"
      AND "#{PG::FTS.table}"."source_id" = "#{source}"."#{key}"
      AND "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_id" = "#{document}"."#{key}"
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_link_delete_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_link_delete_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_link_delete_query.gsub(/^/, '    ')}
        RETURN OLD;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_link_truncate_query
    <<-SQL.gsub(/^ {4}/, '')
    DELETE FROM "#{PG::FTS.table}"
    WHERE "#{PG::FTS.table}"."source_table" = '#{source}'
      AND "#{PG::FTS.table}"."document_table" = '#{document}';
    SQL
  end

  def on_link_truncate_procedure
    <<-SQL.gsub(/^ {4}/, '')
    CREATE FUNCTION "#{on_link_truncate_procedure_name}"()
    RETURNS TRIGGER AS $$
      BEGIN
        #{on_link_truncate_query.gsub(/^/, '    ')}
        RETURN NULL;
      END;
    $$ LANGUAGE plpgsql;
    SQL
  end

  def on_link_insert_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_link_insert_trigger_name}"
    AFTER INSERT ON "#{link}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_link_insert_procedure_name}"();
    SQL
  end

  def on_link_update_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_link_update_trigger_name}"
    AFTER UPDATE ON "#{link}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_link_update_procedure_name}"();
    SQL
  end

  def on_link_delete_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_link_delete_trigger_name}"
    AFTER DELETE ON "#{link}"
    FOR EACH ROW
    EXECUTE PROCEDURE "#{on_link_delete_procedure_name}"();
    SQL
  end

  def on_link_truncate_trigger
    <<-SQL.gsub(/^ {4}/, '')
    CREATE TRIGGER "#{on_link_truncate_trigger_name}"
    AFTER TRUNCATE ON "#{link}"
    FOR EACH STATEMENT
    EXECUTE PROCEDURE "#{on_link_truncate_procedure_name}"();
    SQL
  end
end
