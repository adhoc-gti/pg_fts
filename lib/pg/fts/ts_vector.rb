module PG::FTS::TSVector
  def ts_vector(record = 'NEW')
    record = %("#{record}") unless %w(NEW OLD).include?(record)

    fields.map do |field|
      %(to_tsvector('#{catalog}', COALESCE(#{record}."#{field}", '')))
    end.join(' || ')
  end
end
