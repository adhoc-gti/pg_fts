require 'digest/sha1'

# rubocop:disable Metrics/ModuleLength
module PG::FTS::Naming
  private

  # prefixes

  def on_source_name
    name ? name : "#{source}_to_#{document}"
  end

  def on_document_name
    name ? name : "#{document}_from_#{source}"
  end

  def on_link_name
    name ? name : "#{link}_to_#{document}_from_#{source}"
  end

  def on_source_prefix_unhashed
    name ? suffix(name, :source) : on_source_name
  end

  def on_document_prefix_unhashed
    name ? suffix(name, :document) : on_document_name
  end

  def on_link_prefix_unhashed
    name ? suffix(name, :link) : on_link_name
  end

  def hash_name?
    PG::FTS::Naming.hash_names?
  end

  def hashed(name)
    'sha1_' << (Digest::SHA1.new.tap { |sha| sha << name }).hexdigest
  end

  # rubocop:disable Metrics/CyclomaticComplexity
  def suffix(str, *types)
    str = str.dup

    types.each do |type|
      case type
      when :source   then str << '_src'
      when :link     then str << '_lnk'
      when :document then str << '_doc'
      when :insert   then str << '_ins'
      when :update   then str << '_upd'
      when :delete   then str << '_del'
      when :truncate then str << '_trn'
      when :tsv      then str << '_tsv'
      end
    end

    str
  end
  # rubocop:enable Metrics/CyclomaticComplexity

  def on_source_prefix
    if hash_name?
      suffix(hashed(on_source_name), :source)
    else
      on_source_prefix_unhashed
    end
  end

  def on_document_prefix
    if hash_name?
      suffix(hashed(on_document_name), :document)
    else
      on_document_prefix_unhashed
    end
  end

  def on_link_prefix
    if hash_name?
      suffix(hashed(on_link_name), :link)
    else
      on_link_prefix_unhashed
    end
  end

  # names

  def on_source_insert_name
    suffix(on_source_prefix, :insert, :tsv)
  end

  def on_source_update_name
    suffix(on_source_prefix, :update, :tsv)
  end

  def on_source_delete_name
    suffix(on_source_prefix, :delete, :tsv)
  end

  def on_source_truncate_name
    suffix(on_source_prefix, :truncate, :tsv)
  end

  def on_document_insert_name
    suffix(on_document_prefix, :insert, :tsv)
  end

  def on_document_update_name
    suffix(on_document_prefix, :update, :tsv)
  end

  def on_document_delete_name
    suffix(on_document_prefix, :delete, :tsv)
  end

  def on_document_truncate_name
    suffix(on_document_prefix, :truncate, :tsv)
  end

  def on_link_insert_name
    suffix(on_link_prefix, :insert, :tsv)
  end

  def on_link_update_name
    suffix(on_link_prefix, :update, :tsv)
  end

  def on_link_delete_name
    suffix(on_link_prefix, :delete, :tsv)
  end

  def on_link_truncate_name
    suffix(on_link_prefix, :truncate, :tsv)
  end

  # procedure names

  def on_source_insert_procedure_name
    on_source_insert_name
  end

  def on_source_update_procedure_name
    on_source_update_name
  end

  def on_source_delete_procedure_name
    on_source_delete_name
  end

  def on_source_truncate_procedure_name
    on_source_truncate_name
  end

  def on_document_insert_procedure_name
    on_document_insert_name
  end

  def on_document_update_procedure_name
    on_document_update_name
  end

  def on_document_delete_procedure_name
    on_document_delete_name
  end

  def on_document_truncate_procedure_name
    on_document_truncate_name
  end

  def on_link_insert_procedure_name
    on_link_insert_name
  end

  def on_link_update_procedure_name
    on_link_update_name
  end

  def on_link_delete_procedure_name
    on_link_delete_name
  end

  def on_link_truncate_procedure_name
    on_link_truncate_name
  end

  # trigger names

  def on_source_insert_trigger_name
    on_source_insert_procedure_name
  end

  def on_source_update_trigger_name
    on_source_update_procedure_name
  end

  def on_source_delete_trigger_name
    on_source_delete_procedure_name
  end

  def on_source_truncate_trigger_name
    on_source_truncate_procedure_name
  end

  def on_document_insert_trigger_name
    on_document_insert_procedure_name
  end

  def on_document_update_trigger_name
    on_document_update_procedure_name
  end

  def on_document_delete_trigger_name
    on_document_delete_procedure_name
  end

  def on_document_truncate_trigger_name
    on_document_truncate_procedure_name
  end

  def on_link_insert_trigger_name
    on_link_insert_procedure_name
  end

  def on_link_update_trigger_name
    on_link_update_procedure_name
  end

  def on_link_delete_trigger_name
    on_link_delete_procedure_name
  end

  def on_link_truncate_trigger_name
    on_link_truncate_procedure_name
  end

  module_function

  @hash_names = false

  def hash_names?
    @hash_names
  end

  def hash_names!
    @hash_names = true
  end
end
