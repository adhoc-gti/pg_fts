module PG::FTS::Index
  def create
    [:document, :source, :link].each do |type|
      [:insert, :update, :delete, :truncate].each do |op|
        [:procedure, :trigger].each do |item|
          name = "on_#{type}_#{op}_#{item}"
          yield(send(name)) if respond_to?(name)
        end
      end
    end
  end

  def drop_procedure_query(name)
    <<-SQL.gsub(/^ {4}/, '')
    DROP FUNCTION IF EXISTS "#{send(name + '_name')}" ();
    SQL
  end

  def drop_trigger_query(name, type)
    t = case type
        when :document then document
        when :source then source
        when :link then link
        end

    <<-SQL.gsub(/^ {4}/, '')
    DROP TRIGGER IF EXISTS "#{send(name + '_name')}" ON "#{t}";
    SQL
  end

  def drop
    [:document, :source, :link].each do |type|
      [:insert, :update, :delete, :truncate].each do |op|
        name = "on_#{type}_#{op}_trigger"
        yield(drop_trigger_query(name, type)) if respond_to?(name)
        name = "on_#{type}_#{op}_procedure"
        yield(drop_procedure_query(name)) if respond_to?(name)
      end
    end
  end

  def clear
    yield(on_document_truncate_query)
  end

  def build
    yield(build_query)
  end

  def rebuild(&executor)
    clear(&executor)
    build(&executor)
  end

  class << self
    def drop_all_procedures
      procedures = yield <<-SQL.gsub(/^ {6}/, '')
      SELECT routine_name AS name
      FROM information_schema.routines
      WHERE routine_name LIKE '%_tsv'
        AND routine_type = 'FUNCTION';
      SQL

      procedures.each do |procedure|
        yield <<-SQL.gsub(/^ {8}/, '')
        DROP FUNCTION IF EXISTS "#{procedure['name']}" ();
        SQL
      end
    end

    def drop_all_triggers
      triggers = yield <<-SQL.gsub(/^ {6}/, '')
      SELECT tgname AS name, relname AS table
      FROM pg_trigger
      INNER JOIN pg_class ON pg_class.oid = tgrelid
      WHERE tgname like '%_tsv';
      SQL

      triggers.each do |trigger|
        yield <<-SQL.gsub(/^ {8}/, '')
        DROP TRIGGER IF EXISTS "#{trigger['name']}" ON "#{trigger['table']}";
        SQL
      end
    end

    def drop_all(&executor)
      drop_all_triggers(&executor)
      drop_all_procedures(&executor)
    end

    def create(*indices, &executor)
      indices.each { |index| index.create(&executor) }
    end

    def drop(*indices, &executor)
      indices.each { |index| index.drop(&executor) }
    end

    def clear(*indices, &executor)
      indices.each { |index| index.clear(&executor) }
    end

    def build(*indices, &executor)
      indices.each { |index| index.build(&executor) }
    end

    def rebuild(*indices, &executor)
      indices.each { |index| index.rebuild(&executor) }
    end

    def recreate(*indices, &executor)
      drop_all(&executor)
      create(*indices, &executor)
    end

    def reset(*indices, &executor)
      recreate(*indices, &executor)
      PG::FTS.clear(&executor)
    end
  end

  module Module
    def create(&executor)
      PG::FTS::Index.create(*constants.map { |e| const_get(e) }, &executor)
    end

    def drop(&executor)
      PG::FTS::Index.drop(*constants.map { |e| const_get(e) }, &executor)
    end

    def clear(&executor)
      PG::FTS::Index.clear(*constants.map { |e| const_get(e) }, &executor)
    end

    def build(&executor)
      PG::FTS::Index.build(*constants.map { |e| const_get(e) }, &executor)
    end

    def rebuild(&executor)
      PG::FTS::Index.rebuild(*constants.map { |e| const_get(e) }, &executor)
    end
  end
end

require 'pg/fts/naming'
require 'pg/fts/ts_vector'
require 'pg/fts/index/self'
require 'pg/fts/index/many_to_one'
require 'pg/fts/index/one_to_many'
require 'pg/fts/index/momo'
require 'pg/fts/index/ommo'
