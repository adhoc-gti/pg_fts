# PG::FTS

Relation-aware synchronous full text search index for PostgreSQL using
PL/pgSQL triggers described in and generated via ORM-agnostic Ruby.
