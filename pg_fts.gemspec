Gem::Specification.new do |s|
  s.name        = 'pg_fts'
  s.version     = '0.2.1'
  s.licenses    = ['MIT']
  s.summary     = 'Postgres real-time full text search engine'
  s.description = <<-EOS
  Relation-aware synchronous full text search index for PostgreSQL using PL/pgSQL triggers described in and generated via ORM-agnostic Ruby.
  EOS
  s.authors     = ['Loic Nageleisen']
  s.email       = 'l.nageleisen@adhoc-gti.com'
  s.files       = Dir['lib/**/*.rb']
  s.homepage    = 'https://gitlab.com/adhoc-gti/pg-fts'

  s.add_dependency 'pg', '~> 0.18'
  s.add_development_dependency 'minitest', '~> 5.8.3'
  s.add_development_dependency 'rake', '~> 10.0'
  s.add_development_dependency 'rubocop', '~> 0.36.0'
end
