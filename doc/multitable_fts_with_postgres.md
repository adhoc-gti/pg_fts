# Multi-table Full Text Search with PostgreSQL

## Principles and semantics

A *record* or *row* refers to an entry in a given table structured with
*fields* or *columns*, while a *document* refers to an entity whose data may be
spread across multiple tables. A *source* is the record (or table) from which
the data emanates for a given document.

Let's consider the following example relational schema containing single order
relations:

- `table1` belongs to `table2`
- `table2` has (one|many) `table1`
- `table3` belongs to `table1`
- `table1` has (one|many) `table3`

We want to maintain a full text search index of documents defined as:

    document = (table1.fieldA, table2.fieldB, table3.fieldC, ...)

- `table1` is the *main* table (contains unique id of the document)
- `table2` and `table3` are *related* tables, respectively many-to-one and
  one-to-many relations with respect to `table1`

A single FTS table stores:

- TSV data
- reference to source (as `(table, id)` tuple)
- reference to document (as `(table, id)` tuple)

Note that `source(table+id)+document(table+id)` has a `UNIQUE` constraint.

For each document entry in `table1` there will be in the FTS table:

- one row for `table1`
- zero or one row for `table2`, or exactly one if the relation field cannot be `NULL`
- zero to n rows for `table3`

Each entry in the FTS table needs to be maintained both from its source and its
relation. This is done synchronously via triggers. Note that `table1` is both
source and document, so this fact may be explicitly denoted as *self* in
various places. It is effectively a zero-order relation.

## Situations and scenarios for single order relation test cases

### Strict foreign key consistency constraints

Assuming there are fully implemented foreign key constraints in place
guaranteeing the relations consistency at all times, the rules maintaining
consistency between the FTS table and each source table are simpler to reason
about but have some corner cases that are quick to overlook. This nonetheless
can serve as a gentle introduction to the next chapter as well as a path for
potential optimisation.

#### Consistency of FTS with self

To maintain consistency of the FTS table with respect to `table1` data:

1. changes in table1 data (i.e table1 as a data source) trigger:
   1. on table1 insert: insert fts(table1+id, table1+id) with tsv(table1.fieldA)
   2. on table1 update: update fts(table1+id, table1+id) with tsv(table1.fieldA)
   3. on table1 delete: delete fts(table1+id, table1+id)
   4. on table1 truncate: delete fts(table1, table1)

Note that the parameters are to be duplicated, since `table1` acts as both source
and document. This is important as more tables start to be indexed for which
`table1` may be a source.

#### Consistency of FTS with a one-to-many relation

To maintain consistency of the FTS table with respect to `table3` data:

1. changes in table3 data (i.e table2 as a data source) trigger:
   1. on table3 insert: insert fts(table3+id, table1+id) with tsv(table3.fieldB)
      1. the related id may be NULL, in which case we must not insert
   2. on table3 update: update fts(table3+id, table1+id) with tsv(table3.fieldB, table1.id)
      1. the relation may be updated here, beware of old vs new ids
      2. the new related id may be NULL, so instead: delete fts(table3+id, table1+id)
      3. the old related id may be NULL, so instead: insert fts(table3+id, table1+id) with tsv(table3.fieldB)
      4. since constraint prevents inconsistency, the foreign key will never be
         set to a nonexistent id, hence the record is always kept
   3. on table3 delete, delete fts(table3+id, table1+id)
   4. on table3 truncate, delete fts(table3, table1)
2. changes in table1 (as a relation to the data source) trigger:
   1. on table1 insert: nothing, since constraint prevents inconistent relation
   2. on table1 update: nothing, since relation info is contained in table3
   3. on table1 delete: nothing, since constraint prevents inconistent relation
   4. on table1 truncate: nothing, since constraint prevents inconistent relation

#### Consistency of FTS with a many-to-one relation

To maintain consistency of the FTS table with respect to `table2` data:

1. changes in table2 data (i.e table2 as a data source) trigger:
   1. on table2 insert: nothing, since constraint prevents inconsistent table1 item from preexisting
   2. on table2 update: update fts(table2+id, table1) with tsv(table2.fieldC)
      1. fts update will NOOP if no table1 record is related
      2. fts update can update multiple records
      3. the relation itself cannot be updated here
   3. on table2 delete: nothing, since constraint prevents inconsistent relation
   4. on table2 truncate: nothing, since constraint prevents inconsistent relation
2. changes in table1 (as a relation to the data source) trigger:
   1. on table1 insert: insert fts(table2+id, table1+id) with tsv(table2.fieldC)
   2. on table1 update: update fts(table2+id, table1+id) with tsv(table2.fieldC, table2.id, table1.id)
      1. the relation is being updated here, beware of old vs new ids
      2. tsv must be updated since we are switching records
      3. the relation may be replaced (zero-or-one cardinality)
         1. if the relation cannot be set to NULL, we're done
         2. if it can be set to NULL: instead of update, delete fts(table2+id, table1+id)
         3. it cannot be set to a nonexistent table2.id due to constraints
   3. on table1 delete: delete fts(table2+id, table1+id)
      1. table2.id is redundant in theory since this is a many-to-one relationship
   4. on table1 truncate: delete fts(table2, table1)
      1. table2 is redundant, but we separate concerns

### Relaxed or no foreign key consistency constraints

Now, assuming there are no foreign key constraints in place, or that they can be
relaxed in some cases, it is necessary to fill the blanks as more cases can
occur where we don't want stray entries to persist past a document's lifetime.

#### Consistency of FTS with self

To maintain consistency of the FTS table with respect to `table1` data:

1. changes in table1 data (i.e table1 as a data source) trigger:
   1. on table1 insert: insert fts(table1+id, table1+id) with tsv(table1.fieldA)
   2. on table1 update: update fts(table1+id, table1+id) with tsv(table1.fieldA)
   3. on table1 delete: delete fts(table1+id, table1+id)
   4. on table1 truncate: delete fts(table1, table1)

Note that the parameters are to be duplicated, since table1 acts as both source
and document. See previous chapter.

#### Consistency of FTS with a one-to-many relation

To maintain consistency of the FTS table with respect to `table3` data:

1. changes in table3 data (i.e table3 as a data source) trigger:
   1. on table3 insert:
      1. insert fts(table3+id, table1+id) with tsv(table3.fieldB)
      2. many records may be created, one per table1.id
      3. the related id may be NULL, in which case we must not insert
      4. without constraints, the related id may not exist in table1, in which case we must not insert
   2. on table3 update:
      1. update fts(table3+id, table1+id) with tsv(table3.fieldB, table1.id)
      2. the relation may be updated here, beware of old vs new ids
      3. without constraints, the old related id may be NULL
         1. in which case we must: insert fts(table3.id, table1.id) with tsv(table3.fieldB, table1.id)
         2. but only if the new related id exists
      4. or not exist in table1
         1. in which case we must: insert fts(table3.id, table1.id) with tsv(table3.fieldB, table1.id)
         2. but only if the new related id exists
      5. without constraints, the new related id may be NULL
         1. in which case we must: delete fts(table3.id, table1.id)
      6. or not exist in table1
         1. in which case we must: delete fts(table3.id, table1.id)
   3. on table3 delete, delete fts(table3+id, table1+id)
   4. on table3 truncate, delete fts(table3, table1)
2. changes in table1 (as a relation to the data source) trigger:
   1. on table1 insert: insert fts(table3+id, table1+id) with tsv(table3.fieldB)
      1. we create a fts record from table3 data that preexists related table1 record
      2. nothing should be inserted if there's no related table3.id to be found
   2. on table1 update: nothing, since relation info is contained in table3
      1. the relation itself cannot be updated here (unless id can be updated: unlikely)
   3. on table1 delete: delete fts(table3+id, table1+id)
   4. on table1 truncate: delete fts(table3, table1)

#### Consistency of FTS with a many-to-one relation

To maintain consistency of the FTS table with respect to `table2` data:

1. changes in table2 data (i.e table2 as a data source) trigger:
   1. on table2 insert:
      1. insert fts(table2+id, table1+id) with tsv(table2.fieldC)
      2. many records may be created, one per table1.id
      3. nothing should be inserted if there's no related table1.id to be found
   2. on table2 update:
      1. update fts(table2+id, table1) with tsv(table2.fieldC)
      2. fts update can update multiple records
      3. fts update will NOOP if no table1 record is related
      4. the relation itself cannot be updated here (unless id can be updated: unlikely)
   3. on table2 delete:
      1. delete fts(table2+id, table1+id)
        1. it may affect multiple documents, hence delete many records
        2. join may be simplified into: delete fts(table2+id, table1)
   4. on table2 truncate: delete fts(table2, table1)
2. changes in table1 (as a relation to the data source) trigger:
   1. on table1 insert:
      1. insert fts(table2+id, table1+id) with tsv(table2.fieldC)
      2. a single record is created, but the other ones related to table2.id should not be affected
      3. nothing should be inserted if there's no related table2.id to be found
      4. nothing should be inserted if table2_id is NULL
   2. on table1 update:
      1. update fts(table2+id, table1+id) with tsv(table2.fieldC, table2.id, table1.id)
        1. the relation is being updated here, beware of old vs new ids within the trigger
        2. tsv must be updated since we are switching records
      2. the relation may be replaced (zero-or-one cardinality)
         1. if it was set to NULL, instead of update:
            1. insert fts(table2+id, table1+id) with tsv(table2.fieldC, table2.id, table1.id)
            2. don't insert if table2.id does't exist
         2. if it was set to a nonexistent table2.id, instead of update:
            1. insert fts(table2+id, table1+id) with tsv(table2.fieldC, table2.id, table1.id)
            2. don't insert if table2.id does't exist
         3. if it is being set to NULL: instead of update, delete fts(table2+id, table1+id)
         4. if it is being set to a nonexistent table2.id: instead of update, delete fts(table2+id, table1+id)
   3. on table1 delete:
      1. delete fts(table2+id, table1+id)
        1. table2.id is redundant in theory since this is a many-to-one relationship of which table1 is the bearer
   4. on table1 truncate:
      1. delete fts(table2, table1)
        1. table2 is redundant, but we separate concerns

### Additional principles and semantics for second order relationships

Now that order one relationships are handled, let's have a look at order two
relationships, for which we have to extend previous semantics a bit.

A *document* still refers to an entity whose data may be spread across multiple
tables. A *source* is, as before, the record (or table) from which the data
emanates for a given document. A *link* is the record (or table) that ties the
source and the main table in a *chain* of relations.

Let's start over with a new example structure and consider the following
tables, where `table1` will hold the reference to the document, and `table3`
will hold fields to be indexed, while `table2` ties both together.

```text
| table1            | table2            | table3
+--------           +--------           +--------
| id                | id                | id
|                   |                   | fieldA
|                   |                   | fieldB
|                   |                   | fieldC
```

- `table1` is the *main* table (contains unique id of the *document*)
- `table2` and `table3` are *related* tables
- `table2` is the *link* between `table1` and `table3`
- `table3` is the *source*

The following relationship cases come up:

```text
| table1    |            | table2                |            | table3    | cardinality |
|-------------------------------------------------------------------------|-------------|
| table2_id | >--------- |           | table3_id | >--------- |           |   *-1 *-1   |
|           | ---------< | table1_id | table3_id | >--------- |           |   1-* *-1   |
| table2_id | >--------- |           |           | ---------< | table3_id |   *-1 1-*   |
|           | ---------< | table1_id |           | ---------< | table3_id |   1-* 1-*   |
```

Case 1 (many-to-one many-to-one, or MOMO) is among the most frequent with a
chain of "belongs to" relations. Case 2 (one-to-many many-to-one, or OMMO) is
also quite frequently encoutnered as it is a many-to-many relation. Case 3
(many-to-one one-to-many, or MOOM) happens much less frequently. Case 4
(one-to-many one-to-many, or OMOM) is not the symmetric of case 1 in this
context due to role assymetry between `table1` (main) and `table3` (source).

## Situations and scenarios for second order relation test cases

### Strict foreign key consistency constraints

We're not doing that one.

### Relaxed or no foreign key consistency constraints

#### Consistency of FTS with a MOMO relation

There can be only one `table2` per `table1`, and only one `table3` per `table2`.

1. changes in table3 data (i.e table3 as a data source) trigger:
   1. on table3 insert:
      1. insert fts(table3+id, table1+id) with tsv(table3.fieldC)
      2. many records may be created, one per table1.id
      3. nothing should be inserted if there's no related table2.id to be found
      4. nothing should be inserted if there's no related table1.id to be found via table2
   2. on table3 update:
      1. update fts(table3+id, table1) with tsv(table3.fieldC)
      2. fts update can update multiple records
      3. fts update will NOOP if no table2 record is related
      4. fts update will NOOP if no table1 record is related via table2
      5. the relation itself cannot be updated here (unless id can be updated: unlikely)
   3. on table3 delete:
      1. delete fts(table3+id, table1+id)
        1. it may affect multiple documents, hence delete many records
        2. join may be simplified into: delete fts(table3+id, table1)
   4. on table3 truncate: delete fts(table3, table1)
2. changes in table1 (as a relation to the data source) trigger:
   1. on table1 insert:
      1. insert fts(table3+id, table1+id) with tsv(table3.fieldC)
      2. a single record is created, but the other ones related to table2.id should not be affected
      3. nothing should be inserted if there's no related table2.id to be found
      4. nothing should be inserted if there's no related table3.id to be found via table2.id
      5. nothing should be inserted if table2_id is NULL
      6. nothing should be inserted if table2.table3_id is NULL
   2. on table1 update:
      1. update fts(table3+id, table1+id) with tsv(table3.fieldC, table3.id, table1.id)
        1. the relation is being updated here, beware of old vs new ids within the trigger
        2. tsv must be updated since we are switching records
      2. the relation may be replaced (zero-or-one cardinality)
         1. if it was set to NULL, instead of update:
            1. insert fts(table3+id, table1+id) with tsv(table3.fieldC, table3.id, table1.id)
            2. don't insert if table2.id does't exist
            3. don't insert if table3.id does't exist via existing table2.id
         2. if it was set to a nonexistent table2.id, instead of update:
            1. insert fts(table3+id, table1+id) with tsv(table3.fieldC, table3.id, table1.id)
            2. don't insert if table2.id does't exist
            3. don't insert if table3.id does't exist via existing table2.id
         3. if it is being set to NULL: instead of update, delete fts(table3+id, table1+id)
         4. if it is being set to a nonexistent table2.id: instead of update, delete fts(table3+id, table1+id)
         5. if it is being set to a nonexistent table3.id via table2.id: instead of update, delete fts(table3+id, table1+id)
   3. on table1 delete:
      1. delete fts(table3+id, table1+id)
        1. table3.id is redundant in theory since this is a many-to-one relationship of which table1 is the bearer
   4. on table1 truncate:
      1. delete fts(table3, table1)
        1. table3 is redundant, but we separate concerns
3. changes in table2 (as a link between table1 and table3) trigger:
   1. on table2 insert:
      1. insert fts(table3+id, table1+id) with tsv(table3.fieldC)
      2. many records may be created, one per table1.id
      3. nothing should be inserted if there's no related table3.id to be found
      4. nothing should be inserted if there's no related table1.table2_id to be found
   2. on table2 update:
      1. update fts(table3+id, table1+id) with tsv(table3.fieldC, table3.id, table1.id)
        1. the relation is being updated here, beware of old vs new ids within the trigger
        2. tsv must be updated since we are switching records
      2. the relation may be replaced (zero-or-one cardinality)
         1. if it was set to NULL, instead of update:
            1. insert fts(table3+id, table1+id) with tsv(table3.fieldC, table3.id, table1.id)
            2. don't insert if table3.id does't exist
            3. don't insert if table1.table2_id does't exist
         2. if it was set to a nonexistent table2.id, instead of update:
            1. insert fts(table3+id, table1+id) with tsv(table3.fieldC, table3.id, table1.id)
            2. don't insert if table3.id does't exist
            3. don't insert if table1.table2_id does't exist
         3. if it is being set to NULL: instead of update, delete fts(table3+id, table1+id)
         4. if it is being set to a nonexistent table3.id: instead of update, delete fts(table3+id, table1+id)
   3. on table2 delete:
      1. delete fts(table3+id, table1+id)
        1. it may affect multiple documents, hence delete many records
        2. join may be simplified into: delete fts(table3+id, table1)
   4. on table2 truncate:
      1. delete fts(table3, table1)

#### Consistency of FTS with a OMMO relation

assume (table1_id, table3_id) is unique

1. changes in table3 data (i.e table3 as a data source) trigger:
   1. on table3 insert:
      1. insert if table1 and table3
      2. nothing if no table2
      3. nothing if table2 but no table1
   2. on table3 update:
      1. update tsv
      2. unless no table2
      3. unless table2 but no table1
      4. id change unlikely
   3. on table3 delete:
      1. delete fts
      2. noop if no table2
      3. noop if table2 but no table1
   4. on table3 truncate:
      1. delete fts
2. changes in table1 (as a relation to the data source) trigger:
   1. on table1 insert:
      1. insert if table2 and table3
      2. nothing if no table2
      3. nothing if table2 but no table3
   2. on table1 update:
      1. id change unlikely
   3. on table1 delete:
      1. delete fts
      2. noop if no table2
      3. noop if table2 but no table3
   4. on table1 truncate:
      1. delete fts
3. changes in table2 (as a link between table1 and table3) trigger:
   1. on table2 insert:
      1. insert if table1 and table3
      2. nothing if no table1
      3. nothing if no table3
   2. on table2 update:
      1. update:
        1. if table1_id changes to existing table1 and table3 exists
        2. if table3_id changes to existing table3 and table1 exists
        3. both can change at the same time
      2. delete:
        1. if table1_id changes to non-existing table1
        2. if table1_id changes to null
        3. if table3_id changes to non-existing table3
        4. if table3_id changes to null
      3. insert:
        1. if table1_id changes to existing table1 from non-existing table1
        2. if table1_id changes to existing table1 from null
        3. if table3_id changes to existing table3 from non-existing table3
        4. if table3_id changes to existing table3 from null
      4. noop:
        1. if table1_id changes to null table1 from non-existing table1
        2. if table1_id changes to null table1 from null
        3. if table3_id changes to null table3 from non-existing table3
        4. if table3_id changes to null table3 from null
        5. if table1_id changes to non-existing table1 from non-existing table1
        6. if table1_id changes to non-existing table1 from null
        7. if table3_id changes to non-existing table3 from non-existing table3
        8. if table3_id changes to non-existing table3 from null
   3. on table2 delete:
      1. delete fts
      2. noop if no table1
      3. noop if no table3
   4. on table2 truncate:
      1. delete fts

## References

Lots of interesting things over there:

- [postgres full text search is good enough](http://rachbelaid.com/postgres-full-text-search-is-good-enough/)
- [implementing multi table full text search with postgres](https://robots.thoughtbot.com/implementing-multi-table-full-text-search-with-postgres)
- [caching with postgres materialized views](http://www.matchingnotes.com/caching-with-postgres-materialized-views.html)
- [optimizing full text search with postgres tsvector columns and triggers](https://robots.thoughtbot.com/optimizing-full-text-search-with-postgres-tsvector-columns-and-triggers)

but!

- views for multi-table situations are simple and great, but views are huge
  union/joins, hence slow on big tables, especially with indices on top.
- materialized views precache the view and index, but are slow to build, and
  scheduled only; building index blocks writes.
- make it faster and synchronous by using the usual trigger to fill `tsv`
  straight in model table: useless with multi-tables and numerous relations,
  when we want to discover the relations via the search itself.

This tech:

- is real-time, synchronous, and transactional
- makes relation search easy: find document as well as source entities
- sits purely at DB level: independent of ORM/`schema.rb`, and one less dependency
- is unfortunately complex around trigger building
